import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions de Redux
import { crearNuevaTareaAction } from '../actions/procesosActions';
import { mostrarAlerta, ocultarAlertaAction } from '../actions/alertaActions';

const NuevaTareas = ({history}) => {

    // state del componente
    const [nombre, guardarNombre] = useState('');
    const [descripcion, guardarDescripcion] = useState('');
    const [fechacreacion, guardarFechacreacion] = useState('');
    const [estado, guardarEstadocreacion] = useState('incompleta');

    // utilizar use dispatch y te crea una función
    const dispatch = useDispatch();

    // Acceder al state del store
    const cargando = useSelector( state => state.tareas.loading );
    const error = useSelector(state => state.tareas.error);
    const alerta = useSelector(state => state.alerta.alerta);


    // mandar llamar el action de productoAction
    const agregarTarea = tarea => dispatch( crearNuevaTareaAction(tarea) );

    // cuando el usuario haga submit
    const submitNuevaTarea = e => {
        e.preventDefault();

        // validar formulario
        if(nombre.trim() === '' || descripcion.trim() === '' || fechacreacion.trim() === '') {

            const alerta = {
                msg: 'Todos los campos son obligatorios',
                classes: 'alert alert-danger text-center text-uppercase p3'
            }
            dispatch( mostrarAlerta(alerta) );

            return;
        }

        // si no hay errores
        dispatch( ocultarAlertaAction() );

        // crear el nuevo producto
        agregarTarea({
            nombre,
            descripcion,
            fechacreacion,
            estado
        });

        // redireccionar
        history.push('/');
    }


    return ( 
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar Nueva Tarea
                        </h2>

                        {alerta ? <p className={alerta.classes}> {alerta.msg} </p> : null }

                        <form
                            onSubmit={submitNuevaTarea}
                        >
                            <div className="form-group">
                                <label>Nombre Producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre Producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label>Descripcion</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Descripcion de la tarea"
                                    name="descripcion"
                                    value={descripcion}
                                    onChange={e =>  guardarDescripcion(e.target.value) }
                                />
                            </div>

                            <div className="form-group">
                                <label>Fecha Creacion</label>
                                <input
                                    type="date"
                                    className="form-control"
                                    placeholder="Fecha de Creacion"
                                    name="fechacreacion"
                                    value={fechacreacion}
                                    onChange={e =>  guardarFechacreacion(e.target.value) }
                                />
                            </div>

                            <div className="form-group">
                                <label>Estado Creacion</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Fecha de Creacion"
                                    readonly="readonly"
                                    name="estado"
                                    value={estado}
                                    onChange={e =>  guardarEstadocreacion(e.target.value) }
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                            >Agregar</button>
                        </form>

                        { cargando ? <p>Cargando...</p> : null }
                        
                        { error ? <p className="alert alert-danger p2 mt-4 text-center">Hubo un error</p> : null }
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default NuevaTareas;